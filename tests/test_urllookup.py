'''test for UrlLookup '''

from tempfile import mkdtemp
from topic import Database, UrlLookup
# import pytest


def test_urllookup_empty():
    data = mkdtemp()
    database = Database(data)
    ulu = UrlLookup(database)
    assert ulu.keyvals == {}
    assert database.urllookup.keyvals == {}


def test_urllookup_first():
    '''test first lookup on an empty cache'''
    url = "http://bsd.slashdot.org/story/15/05/01/1455250/openbsd-57-released?utm_source=rss1.0mainlinkanon&utm_medium=feed"
    lurl = "http://bsd.slashdot.org/story/15/05/01/1455250/openbsd-57-released"
    data = mkdtemp()
    database = Database(data)
    rslt = database.urllookup.lookup(url, url)
    assert rslt == lurl
    assert database.urllookup.keyvals == {}
    assert database.urllookup._mru == {url: lurl}

def test_urllookup_second():
    '''test first lookup on a non-empty cache'''
    url = "http://bsd.slashdot.org/story/15/05/01/1455250/openbsd-57-released?utm_source=rss1.0mainlinkanon&utm_medium=feed"
    lurl = "http://bsd.slashdot.org/story/15/05/01/1455250/openbsd-57-released"
    data = mkdtemp()
    database = Database(data)
    _ = database.urllookup.lookup(url, url)
    database.close()

    database = Database(data)
    assert database.urllookup.keyvals == {url: lurl}
    assert database.urllookup._mru == {}
