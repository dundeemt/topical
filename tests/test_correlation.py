'''test the correlation function'''

from topic import correlation


def test_empty_lists():
    l_a = []
    l_b = []
    assert correlation(l_a, l_b) == (0.0, set())

def test_empty_a():
    l_a = []
    l_b = ['foo', ]
    assert correlation(l_a, l_b) == (0.0, set())

def test_empty_b():
    l_a = ['foo', ]
    l_b = []
    assert correlation(l_a, l_b) == (0.0, set())

def test_1_of_5():
    l_a = ['a', ]
    l_b = 'abcde'
    assert correlation(l_a, l_b) == (0.2, set(['a']))

def test_1_of_10():
    l_a = 'afghij'
    l_b = 'abcde'
    assert correlation(l_a, l_b) == (0.1, set(['a']))

def test_4_of_6():
    l_a = 'abcdf'
    l_b = 'abcde'
    assert correlation(l_a, l_b) == (4.0/6.0, set(['a', 'b', 'c', 'd']))

def test_composites_1_of_3():
    l_a = ['a_b', 'c', 'd']
    l_b = ['a_b', ]
    l_a1 = 'abcd'
    l_b1 = 'ab'
    score, _ = correlation(l_a1, l_b1)
    assert correlation(l_a, l_b) == (score, set(['a_b', ]))

def test_composites_1_of_1():
    l_a = ['a_b', ]
    l_b = ['a_b', ]
    l_a1 = 'ab'
    l_b1 = 'ab'
    score, _ = correlation(l_a1, l_b1)
    assert correlation(l_a, l_b) == (score, set(['a_b', ]))

def test_multi_composites():
    l_a = ['a_b_c', 'd']
    l_b = ['a_b_c', ]
    l_a1 = 'abcd'
    l_b1 = 'abc'
    score, _ = correlation(l_a1, l_b1)
    assert correlation(l_a, l_b) == (score, set(['a_b_c', ]))
    assert score == 0.75
