'''tests for the interval parser'''
from datetime import timedelta
import pytest

from topic import delta_from_interval, IntervalError, interval_from_delta

def test_no_subintervals():
    '''test when no subinterval indicators present'''
    with pytest.raises(IntervalError):
        delta_from_interval('12')

def test_seconds():
    '''test standalone seconds'''
    assert delta_from_interval('8s') == timedelta(seconds=8)
def test_minutes():
    '''test standalone minutes'''
    assert delta_from_interval('15m') == timedelta(minutes=15)

def test_hours():
    '''test standalone hours'''
    assert delta_from_interval('3h') == timedelta(hours=3)

def test_hours_minutes():
    '''test hours and minutes'''
    assert delta_from_interval('3h15m') == timedelta(hours=3, minutes=15)

def test_days():
    '''test standalone days'''
    assert delta_from_interval('1d') == timedelta(days=1)

def test_days_hours():
    '''test days and hours'''
    assert delta_from_interval('1d2h') == timedelta(days=1, hours=2)

def test_days_minutes():
    '''test days and minutes'''
    assert delta_from_interval('1d2m') == timedelta(days=1, minutes=2)

def test_days_hours_minutes():
    '''test days, hours and minutes'''
    assert delta_from_interval('1d2h3m') == timedelta(days=1, hours=2, minutes=3)

def test_weeks():
    '''test standalone weeks'''
    assert delta_from_interval('1w') == timedelta(weeks=1)

def test_weeks_days():
    '''test weeks, days'''
    assert delta_from_interval('1w2d') == timedelta(weeks=1, days=2)

def test_weeks_hours():
    '''test weeks and hours'''
    assert delta_from_interval('1w2h') == timedelta(weeks=1, hours=2)

def test_weeks_minutes():
    '''test weeks minutes'''
    assert delta_from_interval('1w2m') == timedelta(weeks=1, minutes=2)

def test_weeks_days_hours():
    '''test weeks, days, hours'''
    assert delta_from_interval('1w2d3h') == timedelta(weeks=1, days=2, hours=3)

def test_weeks_days_minutes():
    '''test weeks, days minutes'''
    assert delta_from_interval('1w2d3m') == timedelta(weeks=1, days=2, minutes=3)

def test_weeks_hours_minutes():
    '''test weeks, hours, minutes'''
    assert delta_from_interval('1w2h3m') == timedelta(weeks=1, hours=2, minutes=3)

def test_unknown_subinterval():
    '''should raise IntervalError'''
    with pytest.raises(IntervalError):
        delta_from_interval('1w3x')

def test_subintervals_out_of_order():
    '''should be msd -> lsd'''
    with pytest.raises(IntervalError):
        delta_from_interval('3d1w2m')

def test_negative_interval():
    '''negative intervals are valid but not helpful'''
    assert delta_from_interval('-1d') == timedelta(days=-1)


def test_to_fro():
    '''test roundtripping'''
    interval = '1w2d3h4m5s'
    assert interval_from_delta(delta_from_interval(interval)) == interval
