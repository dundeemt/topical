'''test the Topics class and it's methods and functions'''
from tempfile import mkdtemp
from topic import Database, Topics


def test_topics_empty():
    '''test that empty allows bind_topics to return the same string'''
    data = mkdtemp()
    database = Database(data)
    topics = Topics(database)
    assert topics.bind_topics('three blind mice') == 'three blind mice'

def test_db_topics():
    '''test that the Database object has a Topics object(topics)'''
    data = mkdtemp()
    database = Database(data)
    assert database.topics.keyvals == {}

def test_bind_topics_simple():
    '''verify simple bind_topics'''
    data = mkdtemp()
    database = Database(data)
    topics = Topics(database)
    topics.add('three blind mice', 'three_blind_mice')
    assert topics.bind_topics('three blind mice') == 'three_blind_mice'


def test_bind_topics_sort():
    '''verify sorted order of bind_topics'''
    data = mkdtemp()
    database = Database(data)
    topics = Topics(database)
    topics.add('three blind', 'three_blind')
    topics.add('three blind mice', 'three_blind_mice')
    assert topics.bind_topics('three blind mice') == 'three_blind_mice'
