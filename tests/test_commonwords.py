'''test for topic module'''

from tempfile import mkdtemp
from topic import Database, CommonWords
# import pytest


# @pytest.mark.parametrize("nput,twords", [
#     ('Harry Shearer Walks Away From "The Simpsons," and $14 Million',
#      [u'harry', u'shearer', u'walks', u'away', u'simpsons', u'$14', u'million']),
# ])
# def test_title_scan(nput, twords):
#     """test title scanning"""
#     assert title_scan(nput) == twords

def test_commonwords_empty():
    data = mkdtemp()
    database = Database(data)
    cwords = CommonWords(database)
    assert cwords.words == []

def test_commonwords_add():
    data = mkdtemp()
    database = Database(data)
    cwords = CommonWords(database)
    the_words = ['a', 'about', 'comes', "doesn't", 'false', 'no', "we'll", 'yes']
    cwords.add(the_words)
    assert cwords.words == the_words
    del cwords
    c1words = CommonWords(database)
    assert c1words.words == the_words

def test_commonwords_delete():
    data = mkdtemp()
    database = Database(data)
    cwords = CommonWords(database)
    the_words = ['a', 'about', 'comes', "doesn't", 'false', 'no', "we'll", 'yes']
    cwords.add(the_words)
    assert cwords.words == the_words
    cwords.delete(['a', "doesn't", 'false', 'yes'])
    assert cwords.words == ['about', 'comes', 'no', "we'll"]
    del cwords
    c1words = CommonWords(database)
    assert c1words.words == ['about', 'comes', 'no', "we'll"]
