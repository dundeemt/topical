'''testing peewee'''

import datetime
import sys

from peewee import SqliteDatabase, Model, CharField
from peewee import ForeignKeyField, DateTimeField, IntegerField

import topic

PW_DB = SqliteDatabase(':memory:')

class BaseModel(Model):
    '''settings common to all who inherit from this class'''
    class Meta:
        database = PW_DB

class Feed(BaseModel):
    '''Rss feed information'''
    title = CharField(unique=True)
    subtitle = CharField()
    url = CharField()
    last_checked = DateTimeField(default=datetime.datetime(1965, 4, 15, 6, 23))
    check_interval_seconds = IntegerField()

    def __unicode__(self):
        return '%s' % self.title


class Article(BaseModel):
    '''article information'''
    feed = ForeignKeyField(Feed, related_name='articles')
    uri = CharField()
    title = CharField()
    summary = CharField()
    discovered = DateTimeField(index=True)

    def __unicode__(self):
        return '%i: %s' % (self.feed.id, self.title)

    class Meta:
        indexes = (
            (('feed', 'uri'), True),
        )

class DbList(object):
    '''base class for persisting a list'''
    def __init__(self, model, sort_fn=None):
        self.model = model
        self._list = []
        self._sort_fn = sort_fn

    def add(self, items_to_add=None):
        '''add all items from the list of items_to_add'''
        items_to_add = items_to_add or list()
        is_dirty = False
        for item in items_to_add:
            self.model.create(item=item)
            is_dirty = True
        if is_dirty:
            self._list = []

    def delete(self, items_to_remove=None):
        '''remove the words from the list'''
        items_to_remove = items_to_remove or list()
        is_dirty = False
        for item in items_to_remove:
            rec = self.model.get(item=item)
            rec.delete_instance()
            is_dirty = True
        if is_dirty:
            self._list = []

    @property
    def items(self):
        '''return the list of items'''
        if not self._list:
            self._list = [m.item for m in self.model.select().order_by(self.model.item)]
            if self._sort_fn is not None:
                self._list.sort(key=self._sort_fn, reverse=True)
        return self._list

class CommonWord(BaseModel):
    '''corpus of commonly occurring words to filter'''
    item = CharField(unique=True)

    def __unicode__(self):
        return '%s' % self.item

class Tagline(BaseModel):
    '''persist taglines to recognize and remove'''
    item = CharField(unique=True)
    # length = IntegerField(index=True)

    def __unicode__(self):
        return '%s' % self.item

class Punctuation(BaseModel):
    '''persist punctuation to be stripped'''
    item = CharField(unique=True)

    def __unicode__(self):
        return '"%s"' % self.item

class DbKeyVal(object):
    '''base class for persisting key-value pairs{dict}'''
    def __init__(self, model):
        self.model = model
        self._dict = {}

    def add(self, key, val):
        '''persist the key:val pair'''
        self.model.create(key=key, val=val)
        self._dict = {}

    def delete(self, key):
        '''remove the key:val pair indicated by key'''
        keyval = self.model.get(key=key)
        keyval.delete_instance()
        self._dict = {}

    @property
    def keyvals(self):
        '''iterate over key, val pairs sorted by len(key) ind descending order'''
        for rec in self.model.select():
            self._dict[rec.key] = rec.val
        skeys = self._dict.keys()
        skeys.sort(key=len, reverse=True)
        for skey in skeys:
            yield skey, self._dict[skey]



class UnicodeChar(BaseModel):
    '''persist Unicode Character replacements'''
    key = CharField(unique=True)
    val = CharField()

    def __unicode__(self):
        return '"%s" -> %s' % (self.key, self.val)


class HtmlEntity(BaseModel):
    '''persist HTML Entity replacements'''
    key = CharField(unique=True)
    val = CharField()

    def __unicode__(self):
        return '"%s" -> %s' % (self.key, self.val)

class Topic(BaseModel):
    '''persist topics and their bindings'''
    key = CharField(unique=True)
    val = CharField()
    length = IntegerField()

    def __unicode__(self):
        return '"%s" -> %s' % (self.key, self.val)

class UrlCache(BaseModel):
    '''cache for UrlLookup'''
    url = CharField(unique=True)
    resolved = CharField()

    def __unicode__(self):
        return '%s -> %s' % (self.url, self.resolved)

PW_DB.connect()
PW_DB.create_tables([Feed, Article, CommonWord, Tagline, Punctuation, UnicodeChar,
                     HtmlEntity, Topic, UrlCache])

dbase = topic.Database()
print "feeds"
for yfeed in dbase.get_feeds():
    feed = Feed.create(title=yfeed.title, subtitle=yfeed.subtitle, url=yfeed.url,
                       last_checked=yfeed.last_checked,
                       check_interval_seconds=yfeed.check_interval)
    print feed

for yfeed in dbase.get_feeds():
    feed = Feed.get(Feed.title == yfeed.title)
    assert yfeed.title == feed.title
    assert yfeed.subtitle == feed.subtitle
    assert yfeed.url == feed.url
    assert yfeed.check_interval == feed.check_interval_seconds

# print "articles"
# for yarticle in dbase.all_articles():
#     a = Article.create(feed=Feed.get(Feed.title == yarticle.feed.title),
#                        uri=yarticle.id,
#                        title=yarticle.title,
#                        summary=yarticle.summary,
#                        discovered=yarticle.dt
#                       )
#     print a

print "common"
commonwords = DbList(CommonWord)
commonwords.add(dbase.common_words.items)

assert dbase.common_words.items == list(cw.item for cw in CommonWord.select())
# print list(cw.word for cw in CommonWord.select())
# print
# print dbase.common_words.items


print commonwords.items

commonwords.add(['zebra', ])
print commonwords.items
commonwords.delete(['zebra', ])
print commonwords.items


print "tagline"
taglines = DbList(Tagline, sort_fn=len)
taglines.add(dbase.taglines.items)

assert dbase.taglines.items == [tl.item for tl in Tagline.select()]
print taglines.items

print "punctuation"
punctuations = DbList(Punctuation)
punctuations.add(dbase.punctuation.items)

assert dbase.punctuation.items == [p.item for p in Punctuation.select()]
print punctuations.items
sys.exit()

print "unicode"
for key, val in dbase.unicodechars.keyvals.iteritems():
    u = UnicodeChar.create(key=key, val=val)
    print u
assert dbase.unicodechars.keyvals == {u.key:u.val for u in UnicodeChar.select()}
# print dbase.unicodechars.keyvals
# print {u.key:u.val for u in UnicodeChar.select()}

print "htmlentity"
for key, val in dbase.htmlentities.keyvals.iteritems():
    h = HtmlEntity.create(key=key, val=val)
    print h
assert dbase.htmlentities.keyvals == {h.key:h.val for h in HtmlEntity.select()}
# print dbase.htmlentities.keyvals
# print {h.key:h.val for h in HtmlEntity.select()}

print "topic"
for key, val in dbase.topics.keyvals.iteritems():
    t = Topic.create(key=key, val=val, length=len(key))
    print t

assert dbase.topics.keyvals == {t.key:t.val for t in Topic.select().order_by(Topic.length.desc())}
# print dbase.topics.keyvals
# print {t.key:t.val for t in Topic.select().order_by(Topic.length).desc()}
# for t in Topic.select().order_by(Topic.length.desc()):
#     print t.length, t.key, t.val

print "urls"
for key, val in dbase.urllookup.keyvals.iteritems():
    u = UrlCache.create(url=key, resolved=val)
    print u
url = 'http://appleinsider.com/articles/15/06/11/apple-to-renovate-flagship-fifth-avenue-store-set-up-temporary-outlet-at-fao-schwarz'

print
print UrlCache.get(UrlCache.url == url)
try:
    print UrlCache.get(UrlCache.url == 'foo')
except UrlCache.DoesNotExist:
    print "'foo' not found"
