"""stage 1 of topical"""

from collections import Counter, OrderedDict
import datetime
from hashlib import md5
from glob import iglob
import os
import string
# import sys
import tempfile
import time
import unicodedata

from backports import statistics
import click
import feedparser
import requests
import yaml


# from textblob import TextBlob

# http://web.resource.org/rss/1.0/modules/syndication/
MIAD = 24 * 60      # Minutes In A Day (MIAD)
SY_TABLE = {
    'hourly': MIAD/24,
    'daily': MIAD,
    'weekly': 7 * MIAD,
    'monthly': 28 * MIAD,
    'yearly': 365 * MIAD
}

class IntervalError(ValueError):
    '''time interval error has occurred'''
    pass

def delta_from_interval(interval):
    """convert an interval NwNdNhNmNs to a timedelta"""
    remain = interval
    weeks = 0
    days = 0
    hours = 0
    minutes = 0
    seconds = 0
    if 'w' in remain:
        weeks, remain = remain.split('w')
        try:
            weeks = int(weeks)
        except ValueError:
            raise IntervalError('Invalid weeks interval [%s]' % weeks)
    if 'd' in remain:
        days, remain = remain.split('d')
        try:
            days = int(days)
        except ValueError:
            raise IntervalError('Invalid days interval [%s]' % days)
    if 'h' in remain:
        hours, remain = remain.split('h')
        try:
            hours = int(hours)
        except ValueError:
            raise IntervalError('Invalid hours interval [%s]' % hours)
    if 'm' in remain:
        minutes, remain = remain.split('m')
        try:
            minutes = int(minutes)
        except ValueError:
            raise IntervalError('Invalid minutes interval [%s]' % minutes)
    if 's' in remain:
        seconds, remain = remain.split('s')
        try:
            seconds = int(seconds)
        except ValueError:
            raise IntervalError('Invalid minutes interval [%s]' % minutes)
    if remain:
        raise IntervalError('Unknown Subinterval[%s]' % remain)

    return datetime.timedelta(weeks=weeks, days=days, hours=hours,
                              minutes=minutes, seconds=seconds)

def interval_from_delta(tdelta):
    """convert a timedelta to an interval format XwYdZhWmVs"""
    weeks, remainder = divmod(tdelta.total_seconds(), 86400 * 7)
    days, remainder = divmod(remainder, 86400)
    hours, remainder = divmod(remainder, 60*60)
    minutes, seconds = divmod(remainder, 60)
    rslt = ''
    if weeks:
        rslt += '%iw' % weeks
    if days:
        rslt += '%id' % days
    if hours:
        rslt += '%ih' % hours
    if minutes:
        rslt += '%im' % minutes
    if seconds:
        rslt += '%is' % seconds
    return rslt


def cloud_klass(word_counts):
    '''convert a Counter object to a a OrderedDict of wordcloud klasses'''
    odct = OrderedDict()
    vals_prime = [x for x in word_counts.values() if x > 1]
    if vals_prime:
        median = statistics.median(vals_prime)
        mean = statistics.mean(vals_prime)
        for key, count in word_counts.most_common():
            if count == 1:
                rslt = 'smallest'
            elif count < median:
                rslt = 'small'
            elif count <= median + mean:
                rslt = 'medium'
            else:
                rslt = 'largest'
            odct[key] = rslt
    return odct


class DbList(object):
    '''base class for persisting a list'''
    def __init__(self, database):
        self.path = os.path.join(database.path, '%s.yaml' % type(self).__name__)
        try:
            with open(self.path) as hfile:
                self._list = yaml.safe_load(hfile)
        except IOError:
            self._list = []

    def save(self):
        '''persist the common words'''
        with open(self.path, 'wb') as hfile:
            yaml.safe_dump(self._list, stream=hfile, default_flow_style=False)

    def add(self, words_to_add=None):
        '''add all words from the list of words_to_add'''
        words_to_add = words_to_add or list()
        self._list += words_to_add
        self._list = list(set(self._list))
        self._list.sort(key=len, reverse=True)
        self.save()

    def delete(self, words_to_remove=None):
        '''remove the words from the list'''
        self._list = list(set(self._list) - set(words_to_remove))
        self._list.sort()
        self.save()

    @property
    def items(self):
        '''return the list of common words'''
        return self._list


class DbKeyVal(object):
    '''base class for persisting key-value pairs{dict}'''
    def __init__(self, database):
        self.path = os.path.join(database.path, '%s.yaml' % type(self).__name__)
        try:
            with open(self.path) as hfile:
                self._dict = yaml.safe_load(hfile)
        except IOError:
            self._dict = {}

    def save(self):
        '''persist the common words'''
        with open(self.path, 'wb') as hfile:
            yaml.safe_dump(self._dict, stream=hfile, default_flow_style=True)

    def add(self, key, val):
        '''add all words from the list of words_to_add'''
        self._dict[key] = val
        self.save()

    def delete(self, key):
        '''remove the words from the list'''
        del self._dict[key]
        self.save()

    @property
    def keyvals(self):
        '''return the dictionary'''
        return self._dict


class UrlLookupError(ValueError):
    '''an error occured lookingup url/eid'''
    pass


class UrlLookups(DbKeyVal):
    '''mechanism to implement a single MRU persistent cache'''
    def __init__(self, database):
        self._mru = {}
        super(UrlLookups, self).__init__(database)

    def lookup(self, article_id, article_link):
        '''peform the lookup and clean up to determine the actual url for the
        article'''
        if not article_id.startswith('http'):
            eid = article_link
        else:
            eid = article_id
        # fish out the real url from news aggregators ala google
        last_http = eid.rindex('http')
        # but watch out for http appearing as part of the article title
        for scheme in ('http://', 'https://'):
            if eid[last_http:].startswith(scheme):
                eid = eid[last_http:]
                break


        # some article_id's are written as a redirect to the actual article so
        # let's chase it down so we can spot duplicates, i'm looking at
        # you techcrunch
        if eid in self._dict:
            self._mru[eid] = self._dict[eid]
            rval = self._dict[eid]
            # print "looked up",
        else:
            # print eid,
            try_count = 0
            while True:
                try_count += 1
                try:
                    rqst = requests.head(eid, allow_redirects=True)
                    if rqst.status_code == 200:
                        break
                    elif rqst.status_code == 403:
                        print 'Status 403:', eid
                        return rqst.url
                    else:
                        print "%i Non200 %i" % (try_count, rqst.status_code)
                        print eid
                except requests.exceptions.TooManyRedirects:
                    # raise UrlLookupError("Too Many Redirects: %s" % eid)
                    print "Too Many Redirects: %s" % eid
                except requests.exceptions.MissingSchema:
                    # raise UrlLookupError("Missing Schema: %s" % eid)
                    print "Missing Schema: %s" % eid
                except Exception as err:
                    print "retrying %i %s" % (try_count, eid)
                    print err

                if try_count >= 3:
                    raise UrlLookupError('Too many retries')

            if '?' in rqst.url:
                rval = rqst.url[:rqst.url.rindex('?')]
            else:
                rval = rqst.url
            rqst.close()
            rqst = None
            self._mru[eid] = rval
            # print "sniffed:",
        return rval

    def save(self):
        if self._mru:
            self._dict = self._mru
            super(UrlLookups, self).save()

    def __del__(self):
        '''call .save to persist _mru to disk'''
        self.save()
        # super(UrlLookups, self).__del__()

    def add(self, key, val):
        self._mru[key] = val
        super(UrlLookups, self).add(key, val)


class CommonWords(DbList):
    '''persist common words list'''
    def __init__(self, database):
        super(CommonWords, self).__init__(database)

    def add(self, words_to_add=None):
        '''add all words from the list of words_to_add'''
        words_to_add = words_to_add or list()
        self._list += words_to_add
        self._list = list(set(self._list))
        self._list.sort()
        self.save()

    def delete(self, words_to_remove=None):
        '''remove the words from the list'''
        self._list = list(set(self._list) - set(words_to_remove))
        self._list.sort()
        self.save()

    @property
    def words(self):
        '''return the list of common words'''
        return self._list


class Taglines(DbList):
    '''persist taglines'''
    def __init__(self, database):
        super(Taglines, self).__init__(database)


class Punctuations(DbList):
    '''persist punctuation marks'''
    def __init__(self, database):
        super(Punctuations, self).__init__(database)


class UnicodeChars(DbKeyVal):
    '''persist Unicode Character replacements'''
    def __init__(self, database):
        super(UnicodeChars, self).__init__(database)


class HtmlEntities(DbKeyVal):
    '''persist HTML Entity replacements'''
    def __init__(self, database):
        super(HtmlEntities, self).__init__(database)

class Topics(DbKeyVal):
    '''persist topics and their bindings'''
    def __init__(self, database):
        super(Topics, self).__init__(database)

    def bind_topics(self, buf):
        '''search buf for topics and bind them together'''
        keys = self._dict.keys()
        keys.sort(key=len, reverse=True)
        for key in keys:
            buf = buf.replace(key, self._dict[key])
        return buf

class Database(object):
    '''database level info'''
    def __init__(self, path='./data'):
        self.path = os.path.abspath(path)
        self.common_words = CommonWords(self)
        self.taglines = Taglines(self)
        self.punctuations = Punctuations(self)
        self.unicodechars = UnicodeChars(self)
        self.htmlentities = HtmlEntities(self)
        self.urllookups = UrlLookups(self)
        self.topics = Topics(self)

    def close(self):
        '''handle graceful cleanup'''
        self.urllookups.save()


    def get_feeds(self):
        """return the list of feeds"""
        for feeddb in [x[0] for x in os.walk(self.path)][1:]:
            # print "path", feeddb
            try:
                with open(os.path.join(self.path, feeddb, 'feed.yaml')) as data:
                    feed = Feed(self)
                    feed.from_yaml(yaml.safe_load(data))
                    yield feed
            except IOError:
                # a non-feed directory, i.e. templates
                pass

    def all_articles(self):
        '''iterator for all articles'''
        for feed in self.get_feeds():
            for article in feed.articles:
                yield article

    def index_dt(self):
        '''articles by datetime'''
        by_date = []
        for article in self.all_articles():
            by_date.append(article)
        by_date.sort(key=lambda x: x.dt, reverse=True)
        for article in by_date:
            yield article

class Feed(object):
    """stucture to manipulate a Feed"""
    def __init__(self, database):
        self.db = database
        self.title = ''
        self.subtitle = ''
        self.url = ''
        self.last_checked = datetime.datetime(1965, 4, 15, 6, 23)
        self.check_interval = 60
        self.path = ''

    def from_url(self, url):
        '''build from from an url'''
        fpr = feedparser.parse(url)
        try:
            self.title = fpr.feed.title
        except AttributeError:
            print fpr.feed
            raise
        dirname = self.title
        try:
            self.subtitle = fpr.feed.subtitle
        except AttributeError:
            self.subtitle = ''
        # if self.subtitle:
        #     dirname += ' - %s' % self.subtitle
        self.path = os.path.join(self.db.path, make_valid_filename(dirname))
        period = SY_TABLE[fpr.feed.get('sy_updateperiod', 'hourly')]
        freq = int(fpr.feed.get('sy_updatefrequency', 1))
        self.url = url
        self.check_interval = period / freq
        os.mkdir(self.path)

    def from_yaml(self, rec):
        """unmarshall the instance data"""
        self.title = rec['title']
        self.subtitle = rec['subtitle']
        self.url = rec['url']
        self.last_checked = rec['last_checked']
        self.check_interval = rec['check_interval']
        dirname = self.title
        # if self.subtitle:
        #     dirname += ' - %s' % self.subtitle
        dirname = unicode(dirname)
        self.path = os.path.join(self.db.path, make_valid_filename(dirname))

    def marshall(self, with_articles=False):
        """gather the info to persist"""
        feedinfo = {
            'title': self.title,
            'subtitle': self.subtitle,
            'url': self.url,
            'last_checked': self.last_checked,
            'check_interval': self.check_interval
        }
        if with_articles:
            feedinfo['articles'] = [article.marshall() for article in self.articles]
        return feedinfo

    def save(self):
        """marshall the instance data"""
        with file(os.path.join(self.path, 'feed.yaml'), 'wb') as hfile:
            yaml.safe_dump(self.marshall(), stream=hfile, default_flow_style=False)

    def fetch(self, verbose=False):
        """get the latest articles for the feed"""
        new = 0
        next_check = self.last_checked
        next_check += datetime.timedelta(minutes=self.check_interval)
        if next_check > datetime.datetime.now():
            if verbose:
                click.echo("%s - Too Soon." % self.title)
            return new

        fpr = feedparser.parse(self.url)
        for entry in fpr.entries:
            try:
                eid = self.db.urllookups.lookup(entry.get('id', entry.link), entry.link)
            except UrlLookupError:
                continue

            summary = entry.get('summary', entry.title)
            try:
                article = Article(self, eid, entry.title, summary)
                if article.save():
                    new += 1
                    # print entry
            except AttributeError:
                print entry
                raise
        self.last_checked = datetime.datetime.now()
        self.save()
        if verbose:
            click.echo("Fetched %s, %i new articles" % (self.title, new))
        return new

    @property
    def articles(self):
        '''return an iterator of Feed's articles'''
        for fname in iglob(os.path.join(self.path, '*.yaml')):
            if fname.endswith('feed.yaml') is False:
                # print fname
                with open(fname) as data:
                    article = Article(self)
                    article.from_yaml(yaml.safe_load(data))
                    yield article

    def __len__(self):
        '''return the number of articles for the feed'''
        num_articles = 0
        for fname in iglob(os.path.join(self.path, '*.yaml')):
            if fname.endswith('feed.yaml') is False:
                num_articles += 1
        return num_articles


class Article(object):
    """stucture to manipulate article for a feed"""
    def __init__(self, feed, id='', title='', summary=''):
        self.feed = feed
        self.id = id
        self.title = title
        self.summary = summary
        self.dt = datetime.datetime.now()
        self._keywords = []
        self.seen = False

    def marshall(self):
        '''prep the data for the object'''
        return {
            'id': self.id,
            'title': self.replace_unicode(self.title),
            'summary': self.summary,
            'dt': self.dt,
        }

    def save(self, overwrite=False):
        '''persist the object data'''
        if not overwrite:
            if os.path.isfile(self.path):
                return 0
        with file(self.path, 'wb') as hfile:
            yaml.safe_dump(self.marshall(), stream=hfile, default_flow_style=False)
        return 1

    @property
    def stripped(self):
        '''summary stripped of html and other troublesome entities'''
        return self.replace_unicode(self.replace_html_entities(strip_html(self.summary)))

    @property
    def path(self):
        '''the full path to the marshalled yaml file for this article'''
        name = self.hash + '.yaml'
        return os.path.join(self.feed.path, name)

    @property
    def hash(self):
        '''the hashed value of the id'''
        return md5(self.id).hexdigest()

    @property
    def keywords(self):
        """return the meaningful words from the title"""
        if not self._keywords:
            buf = strip_html(self.title)
            buf = self.replace_html_entities(buf)
            buf = self.strip_taglines(buf)
            buf = strip_bylines(buf)
            buf = self.replace_unicode(buf)
            buf = buf.lower()
            buf = self.feed.db.topics.bind_topics(buf)
            for word in [word for word in buf.split() if not is_adjective(word)]:
                word = self.strip_punc(strip_possesive(word))
                if word and (word not in self.feed.db.common_words.items):
                    self._keywords.append(word)

        return self._keywords

    def strip_taglines(self, buf):
        """remove unwanted marketing taglines from buffer"""
        taglinez = [
            "Read more of this story at Slashdot.",
            " [&#8230;] Web Design. WordPress. SEO",
            "Ask Slashdot: ",
            "First Click: ",
            "First look: ",
            ]
        if not self.feed.db.taglines.items:
            self.feed.db.taglines.add(taglinez)

        for tagline in self.feed.db.taglines.items:
            buf = buf.replace(tagline, '')

        try:
            tags = buf.split()
            where = tags.index('writes:')
            if where <= 10:
                buf = ' '.join(tags[where+1:])
        except ValueError:
            pass
        return buf

    def strip_punc(self, word):
        """colon, semi-colon, double quotes " and ? marks, period at start/end"""
        puncs = (':', ';', '"', '?', "'", '-', '!', ')', '(', ',')
        if not self.feed.db.punctuations.items:
            self.feed.db.punctuations.add(puncs)

        puncs = tuple(self.feed.db.punctuations.items)
        while word.startswith(puncs):
            word = word[1:]
        while word.endswith(puncs):
            word = word[:-1]
        return word

    def replace_unicode(self, buf):
        """replace troublesome unicode chars"""
        unicode_entities = [
            (u'\u2014', ' '), (u'\_', ' '),
            (u'\u2018', ''), (u'\u2019', ''),
            (u'\u2026', ''),
        ]
        if not self.feed.db.unicodechars.keyvals:
            for key, val in dict(unicode_entities).iteritems():
                self.feed.db.unicodechars.add(key=key, val=val)

        for key, val in self.feed.db.unicodechars.keyvals.iteritems():
            buf = buf.replace(key, val)
        return buf

    def replace_html_entities(self, buf):
        """replace html entity chars"""
        html_entities = [('&amp;', '&'), ('&nbsp;', ' '),
                         ('&ldquo;', '"'), ('&rdquo;', '"'),
                         ('&mdash;', ' '), ('&ndash;', '-'),
                         ('&rsquo;', ''), ('&lsquo;', ''),
                         ('&euro;', ''),
                         ('&pound;', '#'),
                         ('&#8211;', ''), ('&hellip;', '')
                        ]
        if not self.feed.db.htmlentities.keyvals:
            for key, val in dict(html_entities).iteritems():
                self.feed.db.htmlentities.add(key=key, val=val)

        for key, val in self.feed.db.htmlentities.keyvals.iteritems():
            buf = buf.replace(key, val)
        return buf

    def from_yaml(self, yml):
        '''unmarshall from persisted data'''
        self.id = yml['id']
        self.title = self.replace_unicode(yml['title'])
        self.summary = yml['summary']
        self.dt = yml.get('dt', datetime.datetime(2015, 5, 13, 20, 0, 0))


def make_valid_filename(filename):
    '''rip and replace, if possible, invalid characters from a file/directory
    name'''
    valid_fname_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    cleaned_fname = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore')
    return ''.join(c for c in cleaned_fname if c in valid_fname_chars)


def strip_bylines(buf):
    '''strip off (Dopey / Washington Fraud)'''
    if buf.endswith(')'):
        try:
            buf = buf[:buf.rindex('(')].strip()
        except ValueError:
            pass
    return buf


def is_adjective(word):
    '''returns true if adjective'''
    ending = ['ing', 'ly']
    for end in ending:
        if word.endswith(end):
            return True
    return False


def strip_possesive(word):
    """remove possesive 's or ' """
    if word.endswith("'s"):
        return word[:-2]
    elif word.endswith("'"):
        return word[:-1]
    return word



def strip_html(buf):
    """remove html from buf"""
    is_html = False
    for sniff in ['</', '<img']:
        if sniff in buf:
            is_html = True
            continue
    if not is_html:
        return buf

    out = ''
    in_html = 0
    for char in buf:
        if char == '<':
            in_html += 1
        if char == '\n':
            out = out.strip()
            char = ' '
        if not in_html:
            out += char
        if char == '>':
            in_html -= 1
            if in_html == 0:
                out = out.strip()
                out += ' '

    return out


def co_measure(a_set):
    '''return the augmented length of the set, account for composite words'''
    return float(len(a_set) + sum([member.count('_') for member in a_set]))

def correlation(lst_a, lst_b):
    """return a correlation 0.0 -> 1.0 of two lists
    length_of_intersection / length_of_union"""

    intersection = set(lst_a) & set(lst_b)
    union = set(lst_a) | set(lst_b)

    try:
        return co_measure(intersection) / co_measure(union), intersection
    except ZeroDivisionError:
        return 0.0, set([])


@click.group()
@click.option('--db', envvar='TOPICAL_DB', default='./data',
              metavar='PATH',
              help='specify the db path. (./data)')
@click.option('--verbose', '-v', is_flag=True,
              help='Enables verbose mode.')
@click.version_option('0.0.1')
@click.pass_context
def cli(ctx, db, verbose):
    """base command actions"""
    db = os.path.abspath(db)
    ctx.obj = {}
    ctx.obj['db'] = Database(db)
    ctx.obj['verbose'] = verbose


@click.command()
@click.option('--repeat', is_flag=True)
@click.option('--interval', default='15m',
              help='TimeInterval sleep between fetches')
@click.pass_context
def fetch(ctx, repeat, interval):
    """fetch new articles from your feeds"""
    delta_interval = delta_from_interval(interval)
    while True:
        article_count = 0
        for feed in ctx.obj['db'].get_feeds():
            article_count += feed.fetch(verbose=ctx.obj['verbose'])
        if article_count:
            click.echo("\n%i New Artcles" % article_count)
        if not repeat:
            break
        click.echo("sleeping %s ...(%s)" % (interval, datetime.datetime.now()))
        time.sleep(delta_interval.seconds)


@click.command()
@click.option('--recent', default='1h',
              help='articles in the past TimeInterval')
@click.option('--retro', default='1d',
              help='TimeInterval to look back for related articles')
@click.pass_context
def related(ctx, recent, retro):
    '''using stories in the past --recent show related articles --retro back
    TimeInterval is a string in the form XwYdZhWm, where XYZW are all integers
    specifying (w)eeks, (d)ays, (h)ours and (m)inutes. 1w2d3h4m is 1 week,
    2 days 3 hours and 4 minutes.  A time interval of '3h' is 3 hours.'''
    now = datetime.datetime.now()
    ctx.obj['now'] = now
    ctx.obj['recent'] = recent
    ctx.obj['retro'] = retro
    delta_recent = delta_from_interval(recent)
    delta_retro = delta_from_interval(retro)
    recs = []
    isects = Counter()
    words = Counter()
    articles = list(ctx.obj['db'].index_dt())
    for article in articles:
        if now - article.dt > delta_recent:
            # print 'break', now, article.dt, delta_recent
            break
        # print now, article.dt, delta_recent
        cousins = []
        article_ids = [article.id]
        for other in articles:
            if hasattr(other, 'interval'):
                other.interval[article.hash] = interval_from_delta(article.dt - other.dt)
            else:
                other.interval = {article.hash: interval_from_delta(article.dt - other.dt)}
            in_retro_window = article.dt - other.dt <= delta_retro
            if (other.dt < article.dt) and in_retro_window:
                co_score, isect = correlation(article.keywords, other.keywords)
                if (co_measure(isect) == 1) and (co_score <= 2.0):
                    #ignore single word matches with low scores
                    continue
                if (co_score > 0.143) or (co_measure(isect) > 2):
                    if other.id in article_ids:
                        # print "Found Duplicate: %s" % other.id
                        continue
                    else:
                        cousins.append((co_score, isect, other))
                        article_ids.append(other.id)
        cousins.sort(key=lambda x: x[0], reverse=True)
        recs.append((article, cousins))

    recs = filter_complete_subsets(recs)
    seen = []
    for article, cousins in recs:
        if article.hash in seen:
            article.seen = True
        else:
            seen.append(article.hash)
        for cousin in cousins:
            _, isect, other = cousin
            if other.hash in seen:
                other.seen = True
            else:
                seen.append(other.hash)
            words.update(isect)
            isects.update([' '.join(isect),])
    webify(recs, isects=cloud_klass(isects), words=cloud_klass(words), ctx=ctx)


def filter_complete_subsets(recz):
    '''filter out records where the article and all cousins are found in a
    prior record's cousins'''
    stop = len(recz) - 1
    for ptr in range(stop, -1, -1):
        article, cousins = recz[ptr]
        for rec in recz:
            oarticle, ocousins = rec
            if article == oarticle:
                if article.feed == oarticle.feed:
                    break
            carticles = [x[2] for x in ocousins]
            # print "looking for %s in %s" % (article, carticles)
            if article in carticles:
                found = True

                # print "found: %s under: %s" % (article, oarticle)
                for cousin in cousins:
                    if cousin[2] not in carticles:
                        # print "%s not in %s" % (cousin[2], carticles)
                        found = False
                        break
                if found:
                    # print "dumping:", recz[ptr]
                    del recz[ptr]
                    break
    return recz


def webify(recz, isects, words, ctx):
    '''do it with jinja'''
    import jinja2
    tpath = os.path.join(ctx.obj['db'].path, 'templates')
    template_loader = jinja2.FileSystemLoader(searchpath=tpath)
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template("report.html")

    template_vars = {
        "recs": recz,
        "now": ctx.obj['now'],
        "recent": ctx.obj['recent'],
        "retro": ctx.obj['retro'],
        "isects": isects,
        "words": words,
    }

    fhndl, fname = tempfile.mkstemp(suffix='.html')
    os.write(fhndl, template.render(template_vars).encode('utf-8'))
    os.close(fhndl)
    click.launch('file://%s' % fname)


def ferret_taglines(database):
    '''find nasty taglines that cloud the results and output the most egregious ones'''
    hangers = []
    for article in database.all_articles():
        if ' - ' in article.title:
            found_at = article.title.rindex(' - ')
            tagline = article.title[found_at:]
            if tagline in database.taglines.items:
                continue
            else:
                hangers.append(tagline)

    hangers = Counter(hangers)
    cmd = '\ttopical taglines --insert="%s"'
    result = [key for key, count in hangers.iteritems() if count > 1]
    if result:
        click.echo("Use the following cmd to quiet these noisey bits.")
        click.echo(cmd % '","'.join(result))
    else:
        click.echo("No interesting bits found.")


@click.command()
@click.option('--insert', default='',
              help='tagline to add')
@click.option('--delete', default='',
              help='tagline to delete')
@click.option('--display', is_flag=True)
@click.option('--ferret', is_flag=True)
@click.pass_context
def taglines(ctx, insert, delete, display, ferret):
    '''manipulate taglines list'''
    db_list = ctx.obj['db'].taglines
    if insert:
        items = insert.split(',')
        db_list.add(items)
    if delete:
        items = delete.split(',')
        db_list.delete(items)
    if display:
        for item in db_list.items:
            click.echo(item)
    if ferret:
        ferret_taglines(database=ctx.obj['db'])


@click.command()
@click.option('--insert', default='',
              help='commonword(s) to add')
@click.option('--delete', default='',
              help='commonword(s) to delete')
@click.option('--display', is_flag=True)
@click.pass_context
def commonwords(ctx, insert, delete, display):
    '''manipulate common words list'''
    db_list = ctx.obj['db'].common_words
    if insert:
        items = insert.split(',')
        db_list.add(items)
    if delete:
        items = delete.split(',')
        db_list.delete(items)
    if display:
        for item in db_list.items:
            click.echo(item)

@click.command()
@click.option('--insert', default='',
              help='topic(s) to add k1:v1,k2:v2,...')
@click.option('--delete', default='',
              help='topic(s) to delete k1,k2,...')
@click.option('--display', is_flag=True)
@click.pass_context
def topics(ctx, insert, delete, display):
    '''manipulate topic bindings keyvals'''
    db_keyvals = ctx.obj['db'].topics
    if insert:
        items = insert.split(',')
        for item in items:
            key, val = item.split(':')
            db_keyvals.add(key=key, val=val)
    if delete:
        for item in delete.split(','):
            db_keyvals.delete(item)
    if display:
        for key, val in db_keyvals.keyvals.iteritems():
            click.echo('%s->%s' % (key, val))


@click.command()
@click.option('--articles', is_flag=True)
@click.pass_context
def export(ctx, articles):
    '''export the database'''
    db = ctx.obj['db']
    database = {
        'CommonWords': db.common_words.items,
        'HtmlEntities': db.htmlentities.keyvals,
        'Punctuations': db.punctuations.items,
        'Taglines': db.taglines.items,
        'UnicodeChars': db.unicodechars.keyvals,
        'feeds': list(f.marshall(with_articles=articles) for f in db.get_feeds())
    }
    click.echo(yaml.safe_dump(database))

@click.command()
@click.option('--insert', default='',
              help='url(s) of feeds to add. url1,url2,...')
@click.option('--delete', default='',
              help='feed name(s) to delete name1,name2,...')
@click.option('--display', is_flag=True,
              help='list feeds')
@click.option('--opml', default='',
              help='import feeds from opml file')
@click.pass_context
def feeds(ctx, insert, delete, display, opml):
    '''manage RSS feeds'''
    database = ctx.obj['db']
    if insert:
        for url in insert.split(','):
            click.echo('adding url: %s' % url)
            feed = Feed(database)
            feed.from_url(url)
            feed.save()

    if delete:
        pass
    if display:
        feed_count = 0
        article_count = 0
        for feed in database.get_feeds():
            title = '"%s"' % feed.title
            click.echo(title.ljust(34) + "\t(%i articles)" % len(feed))
            feed_count += 1
            article_count += len(feed)
        click.echo("%i Feeds with %i articles" % (feed_count, article_count))

    if opml:
        import listparser
        lprs = listparser.parse(file(opml).read())
        if lprs.bozo:
            print "there was a problem parsing %s" % opml
            print lprs.bozo_exception
        for element in lprs.feeds:
            cnfrm = click.confirm("Add %s ?" % element.title, default=True)
            if cnfrm:
                print "adding", element.title, element.url

cli.add_command(fetch)
cli.add_command(feeds)

cli.add_command(related)
cli.add_command(taglines)
cli.add_command(commonwords)
cli.add_command(topics)
cli.add_command(export)

if __name__ == '__main__':
    cli(obj={})

