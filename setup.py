from setuptools import setup

setup(
    name='topical',
    version='0.0.1',
    py_modules=['topic'],
    install_requires=[
        'Click',
        'pyyaml',
        'feedparser'
    ],
    entry_points='''
        [console_scripts]
        topical=topic:cli
    ''',
)
